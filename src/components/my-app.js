import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';

import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';

import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-iconset-svg/iron-iconset-svg.js';

import '@polymer/paper-icon-button/paper-icon-button.js';

import './app-avatar';

const template = html`
<iron-iconset-svg name="app" size="24">
    <svg>
        <defs>
            <g id="menu">
                <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
            </g>
        </defs>
    </svg>
</iron-iconset-svg>`

document.head.appendChild(template.content);

class MyApp extends PolymerElement {
    static get template() {
        return html`
            <style>

                :host {
                    --app-primary-color: #404040;
                    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                }

                app-drawer app-header {
                    background-color: var(--app-primary-color);
                }

                app-toolbar {
                    height: 60px;
                    border-bottom: 1px solid #ddd;
                }

                .main-header {
                    background-color: #fff;
                    color: #444;
                }

                .title-toolbar {
                    @apply --layout-center-center;
                    box-sizing: border-box;
                }

                .nav-title-toolbar {
                    color: #fff;
                    width: 100vw;
                }

                .main-title-toolbar {
                    width: 100%;
                }

                .title {
                    padding-bottom: 40px;
                    font-size: 60px;
                    font-weight: 800;
                }

                [hidden] {
                    display: none;
                }

                @media (max-width: 580px) {
                /* make title smaller to fit on screen */
                    .title {
                        font-size: 40px;
                        padding-bottom: 16px;
                    }
                }

            </style>

            <iron-media-query query="max-width: 1024px" query-matches="{{smallScreen}}"></iron-media-query>

            <template is='dom-if' if='[[smallScreen]]'>
                <!-- Todo: add drawer mechanism. -->
            </template>

            <!--  -->
            <app-header-layout>
                <app-header fixed effects="waterfall" class="main-header" slot="header">

                    <app-toolbar>
                        <paper-icon-button drawer-toggle icon="app:menu"></paper-icon-button>
                        
                        <!-- Todo: add a search bar -->
                        <div main-title>App name</div>
                        
                        <!-- Todo: add a notification icon with a badge -->
                        <app-avatar user=''></app-avatar>
                    </app-toolbar>

                    <!-- <app-toolbar class="title-toolbar main-title-toolbar">
                        <div class="title">ZUPERKÜLBLOG</div>
                    </app-toolbar> -->


                </app-header>
            </app-header-layout>
        `;
    }
}

window.customElements.define('my-app', MyApp);
