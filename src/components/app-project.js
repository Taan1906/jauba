import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-repeat';
import './app-card';

window.customElements.define('app-project', class AppProject extends PolymerElement {
    static get properties() {
        return {
            projects: {
                value() {
                    return [
                        {
                            author: '', // project's author
                            created_at: 1, // project created at
                            description: '', // project description
                            name: '', // project name
                        }
                    ]
                }
            }
        };
    }

    static get template() {
        return html`
            <style>
                .gcontainer {
                    display: grid;
                    grid-template-columns: auto auto auto;
                    background-color: #2196F3;
                    padding: 10px;
                }

                /* ========================================================================
                    Component: Grid
                 ========================================================================== */
                /*
                 * 1. Allow cells to wrap into the next line
                 * 2. Reset list
                 */
                .grid {
                    width: 100%;
                    display: flex;
                    /* 1 */
                    flex-wrap: wrap;
                    /* 2 */
                    margin: 0;
                    padding: 0;
                    list-style: none;
                }
                /*
                 * Grid cell
                 * Note: Space is allocated solely based on content dimensions, but shrinks: 0 1 auto
                 * Reset margin for e.g. paragraphs
                 */
                .grid > * {
                    margin: 0;
                }
                /*
                 * Remove margin from the last-child
                 */
                .grid > * > :last-child {
                    margin-bottom: 0;
                }
                /* Gutter
                ========================================================================== */
                /*
                 * Default
                 */
                /* Horizontal */
                .grid {
                    margin-right: -30px;
                }
                .grid > * {
                    padding-right: 30px;
                }
                /* Vertical */
                .grid + .grid,
                .grid > .grid-margin,
                * + .grid-margin {
                    margin-top: 30px;
                }
                /* Desktop and bigger */
                @media (min-width: 1200px) {
                    /* Horizontal */
                    .grid {
                        margin-right: -40px;
                    }
                    .grid > * {
                        padding-right: 40px;
                    }
                    /* Vertical */
                    .grid + .grid,
                    .grid > .grid-margin,
                    * + .grid-margin {
                        margin-top: 40px;
                    }
                }
                /*
                 * Small
                 */
                /* Horizontal */
                .grid-small,
                .grid-column-small {
                    margin-right: -15px;
                }
                .grid-small > *,
                .grid-column-small > * {
                    padding-right: 15px;
                }
                /* Vertical */
                .grid + .grid-small,
                .grid + .grid-row-small,
                .grid-small > .grid-margin,
                .grid-row-small > .grid-margin,
                * + .grid-margin-small {
                    margin-top: 15px;
                }
                /*
                 * Medium
                 */
                /* Horizontal */
                .grid-medium,
                .grid-column-medium {
                    margin-right: -30px;
                }
                .grid-medium > *,
                .grid-column-medium > * {
                    padding-right: 30px;
                }
                /* Vertical */
                .grid + .grid-medium,
                .grid + .grid-row-medium,
                .grid-medium > .grid-margin,
                .grid-row-medium > .grid-margin,
                * + .grid-margin-medium {
                    margin-top: 30px;
                }
                /*
                 * Large
                 */
                /* Horizontal */
                .grid-large,
                .grid-column-large {
                    margin-right: -40px;
                }
                .grid-large > *,
                .grid-column-large > * {
                    padding-right: 40px;
                }
                /* Vertical */
                .grid + .grid-large,
                .grid + .grid-row-large,
                .grid-large > .grid-margin,
                .grid-row-large > .grid-margin,
                * + .grid-margin-large {
                    margin-top: 40px;
                }
                /* Desktop and bigger */
                @media (min-width: 1200px) {
                    /* Horizontal */
                    .grid-large,
                    .grid-column-large {
                        margin-right: -70px;
                    }
                    .grid-large > *,
                    .grid-column-large > * {
                        padding-right: 70px;
                    }
                    /* Vertical */
                    .grid + .grid-large,
                    .grid + .grid-row-large,
                    .grid-large > .grid-margin,
                    .grid-row-large > .grid-margin,
                    * + .grid-margin-large {
                        margin-top: 70px;
                    }
                }
                /*
                 * Collapse
                 */
                /* Horizontal */
                .grid-collapse,
                .grid-column-collapse {
                    margin-right: 0;
                }
                .grid-collapse > *,
                .grid-column-collapse > * {
                    padding-right: 0;
                }
                /* Vertical */
                .grid + .grid-collapse,
                .grid + .grid-row-collapse,
                .grid-collapse > .grid-margin,
                .grid-row-collapse > .grid-margin {
                    margin-top: 0;
                }
                /* Divider
                ========================================================================== */
                .grid-divider > * {
                    position: relative;
                }
                .grid-divider > :not(.uk-first-column)::before {
                    content: "";
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    border-right: 1px solid #e5e5e5;
                }
                /* Vertical */
                .grid-divider.grid-stack > .grid-margin::before {
                    content: "";
                    position: absolute;
                    right: 0;
                    left: 0;
                    border-top: 1px solid #e5e5e5;
                }
                /*
                * Default
                */
                /* Horizontal */
                .grid-divider {
                    margin-right: -60px;
                }
                .grid-divider > * {
                    padding-right: 60px;
                }
                .grid-divider > :not(.uk-first-column)::before {
                    right: 30px;
                }
                /* Vertical */
                .grid-divider.grid-stack > .grid-margin {
                    margin-top: 60px;
                }
                .grid-divider.grid-stack > .grid-margin::before {
                    top: -30px;
                    right: 60px;
                }
                /* Desktop and bigger */
                @media (min-width: 1200px) {
                    /* Horizontal */
                    .grid-divider {
                        margin-right: -80px;
                    }
                    .grid-divider > * {
                        padding-right: 80px;
                    }
                    .grid-divider > :not(.uk-first-column)::before {
                        right: 40px;
                    }
                    /* Vertical */
                    .grid-divider.grid-stack > .grid-margin {
                        margin-top: 80px;
                    }
                    .grid-divider.grid-stack > .grid-margin::before {
                        top: -40px;
                        right: 80px;
                    }
                }
                /*
                 * Small
                 */
                /* Horizontal */
                .grid-divider.grid-small,
                .grid-divider.grid-column-small {
                    margin-right: -30px;
                }
                .grid-divider.grid-small > *,
                .grid-divider.grid-column-small > * {
                    padding-right: 30px;
                }
                .grid-divider.grid-small > :not(.uk-first-column)::before,
                .grid-divider.grid-column-small > :not(.uk-first-column)::before {
                    right: 15px;
                }
                /* Vertical */
                .grid-divider.grid-small.grid-stack > .grid-margin,
                .grid-divider.grid-row-small.grid-stack > .grid-margin {
                    margin-top: 30px;
                }
                .grid-divider.grid-small.grid-stack > .grid-margin::before {
                    top: -15px;
                    right: 30px;
                }
                .grid-divider.grid-row-small.grid-stack > .grid-margin::before {
                    top: -15px;
                }
                .grid-divider.grid-column-small.grid-stack > .grid-margin::before {
                    right: 30px;
                }
                /*
                 * Medium
                 */
                /* Horizontal */
                .grid-divider.grid-medium,
                .grid-divider.grid-column-medium {
                    margin-right: -60px;
                }
                .grid-divider.grid-medium > *,
                .grid-divider.grid-column-medium > * {
                    padding-right: 60px;
                }
                .grid-divider.grid-medium > :not(.uk-first-column)::before,
                .grid-divider.grid-column-medium > :not(.uk-first-column)::before {
                    right: 30px;
                }
                /* Vertical */
                .grid-divider.grid-medium.grid-stack > .grid-margin,
                .grid-divider.grid-row-medium.grid-stack > .grid-margin {
                    margin-top: 60px;
                }
                .grid-divider.grid-medium.grid-stack > .grid-margin::before {
                    top: -30px;
                    right: 60px;
                }
                .grid-divider.grid-row-medium.grid-stack > .grid-margin::before {
                    top: -30px;
                }
                .grid-divider.grid-column-medium.grid-stack > .grid-margin::before {
                    right: 60px;
                }
                /*
                 * Large
                 */
                /* Horizontal */
                .grid-divider.grid-large,
                .grid-divider.grid-column-large {
                    margin-right: -80px;
                }
                .grid-divider.grid-large > *,
                .grid-divider.grid-column-large > * {
                    padding-right: 80px;
                }
                .grid-divider.grid-large > :not(.uk-first-column)::before,
                .grid-divider.grid-column-large > :not(.uk-first-column)::before {
                    right: 40px;
                }
                /* Vertical */
                .grid-divider.grid-large.grid-stack > .grid-margin,
                .grid-divider.grid-row-large.grid-stack > .grid-margin {
                    margin-top: 80px;
                }
                .grid-divider.grid-large.grid-stack > .grid-margin::before {
                    top: -40px;
                    right: 80px;
                }
                .grid-divider.grid-row-large.grid-stack > .grid-margin::before {
                    top: -40px;
                }
                .grid-divider.grid-column-large.grid-stack > .grid-margin::before {
                    right: 80px;
                }
                /* Desktop and bigger */
                @media (min-width: 1200px) {
                    /* Horizontal */
                    .grid-divider.grid-large,
                    .grid-divider.grid-column-large {
                        margin-right: -140px;
                    }
                    .grid-divider.grid-large > *,
                    .grid-divider.grid-column-large > * {
                        padding-right: 140px;
                    }
                    .grid-divider.grid-large > :not(.uk-first-column)::before,
                    .grid-divider.grid-column-large > :not(.uk-first-column)::before {
                        right: 70px;
                    }
                    /* Vertical */
                    .grid-divider.grid-large.grid-stack > .grid-margin,
                    .grid-divider.grid-row-large.grid-stack > .grid-margin {
                        margin-top: 140px;
                    }
                    .grid-divider.grid-large.grid-stack > .grid-margin::before {
                        top: -70px;
                        right: 140px;
                    }
                    .grid-divider.grid-row-large.grid-stack > .grid-margin::before {
                        top: -70px;
                    }
                    .grid-divider.grid-column-large.grid-stack > .grid-margin::before {
                        right: 140px;
                    }
                }
                /* Match child of a grid cell
                ========================================================================== */
                /*
                * Behave like a block element
                * 1. Wrap into the next line
                * 2. Take the full width, at least 100%. Only if no class from the Width component is set.
                * 3. Expand width even if larger than 100%, e.g. because of negative margin (Needed for nested grids)
                */
                .grid-match > *,
                .grid-item-match {
                    display: flex;
                    /* 1 */
                    flex-wrap: wrap;
                }
                .grid-match > * > :not([class*='width']),
                .grid-item-match > :not([class*='width']) {
                    /* 2 */
                    box-sizing: border-box;
                    width: 100%;
                    /* 3 */
                    flex: auto;
                }

                .grid>div { width: 300px; }
            </style>

            <h1>Projects</h1>

            <div class='grid'>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
                <div class="width-auto@m">
                    <app-card>
                        <h3 slot='title'>Small</h3>
                        <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </app-card>
                </div>
            </div>

            <!-- <div class='gcontainer'>
                <template>
                    <dom-repeat items="{{projects}}">
                        <template>
                            <app-card>
                                <h3 slot='title'>Small</h3>
                                <p slot='content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </app-card>
                            <paper-card heading="[[item.name]]">
                                <div class="card-content"></div>
                                <div class="card-actions">
                                    <paper-button>Some action</paper-button>
                                </div>
                            </paper-card>
                        </template>
                    </dom-repeat>
                </template>
            </div> -->
        `;
    }
});