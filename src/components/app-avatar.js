import { PolymerElement, html } from "@polymer/polymer";

class Avatar extends PolymerElement {
    static get properties() {
        return {
            user: {
                type: String,
                notify: true
            }
        }
    }

    static get template() {
        return html`
            <style>
                .user-avatar {
                    width: 32px;
                    height: 32px;
                    border-radius: 50px;
                    cursor: pointer;
                    outline: none;
                }
            </style>

            <img class='user-avatar' src='./images/manifest/icon-48x48.png' />

            <!--
                Todo: add action box on click event on
                      avatar, show user details, and
                      user action buttons 'settings'
                      and 'logout'.
            -->
        `;
    }
}

window.customElements.define('app-avatar', Avatar);
