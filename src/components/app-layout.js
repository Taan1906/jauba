import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/app-layout/app-header/app-header.js';

window.customElements.define('app-layout', class App extends PolymerElement {
    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                }

                .container {
                    display: flex;
                    flex-direction: column;
                    min-height: 100vh;
                }


                @media (min-width: 768px) {
                    .container {
                        display: grid;
                        grid-template-columns: 60px 1fr 200px;
                        grid-template-rows: 60px 1fr auto;
                        grid-template-areas:
                            'nav header header'
                            'nav main main';
                    }
                }

                .header {
                    text-align: center;
                    background-color: #369;
                    color: #fff;
                    grid-area: header;
                    width: 100%;
                    height: 40px;
                    padding: 10px;
                    position: fixed;
                }

                .main {
                    flex: 1;
                    padding: 20px;
                    grid-area: main;
                }

                .nav {
                    background-color: #f90;
                    padding: 10px;
                    grid-area: nav;
                    position: fixed;
                    height: 100%;
                    width: 40px;
                }
            </style>

            <div class='container'>
                <app-header class='header' fixed effects="waterfall">
                    <slot name='header'></slot>
                </app-header>

                <div class='nav'>
                    <slot name='nav'></slot>
                </div>
                
                <div class='main'>
                    <slot name='main'></slot>
                </div>
            </div>
        `;
    }
});
