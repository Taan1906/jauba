import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import './app-layout';
import './app-project';

window.customElements.define('my-app', class MyApp extends PolymerElement {
    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                    background-attachment: fixed;
                    background-size: 100% auto;
                    background-repeat: no-repeat;
                }

            </style>

            <div class='toolbar'>
                
            </div>
        `;
    }
});
