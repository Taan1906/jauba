import { PolymerElement } from "@polymer/polymer";
import '@polymer/app-storage/app-network-status-behavior';
import { mixinBehaviors } from '@polymer/polymer/lib/legacy/class.js';
import '@polymer/polymer/polymer-legacy.js';

class FirebaseApp extends PolymerElement {
    static get properties() {
        return {
            /**
           * The name of your app. Optional.
           *
           * You can use this with the `appName` property of other Polymerfire elements
           * in order to use multiple firebase configurations on a page at once.
           * In that case the name is used as a key to lookup the configuration.
           */
            name: {
                type: String,
                value: ''
            },

            /**
             * Your API key.
             *
             * Get this from the Auth > Web Setup panel of the new
             * Firebase Console at https://console.firebase.google.com
             *
             * It looks like this: 'AIzaSyDTP-eiQezleFsV2WddFBAhF_WEzx_8v_g'
             */
            apiKey: {
                type: String
            },

            /**
             * The domain name to authenticate with.
             *
             * The same as your Firebase Hosting subdomain or custom domain.
             * Available on the Firebase Console.
             *
             * For example: 'polymerfire-test.firebaseapp.com'
             */
            authDomain: {
                type: String
            },

            /**
             * The URL of your Firebase Realtime Database. You can find this
             * URL in the Database panel of the Firebase Console.
             * Available on the Firebase Console.
             *
             * For example: 'https://polymerfire-test.firebaseio.com/'
             */
            databaseUrl: {
                type: String
            },

            /**
             * The Firebase Storage bucket for your project. You can find this
             * in the Firebase Console under "Web Setup".
             *
             * For example: `polymerfire-test.appspot.com`
             */
            storageBucket: {
                type: String,
                value: null
            },

            /**
             * The Firebase Cloud Messaging Sender ID for your project. You can find
             * this in the Firebase Console under "Web Setup".
             */
            messagingSenderId: {
                type: String,
                value: null
            },

            /**
             * The Google Cloud Project ID for your project. You can find this
             * in the Firebase Console under "Web Setup".
             *
             * For example: `polymerfire-test`
             */
            projectId: {
                type: String,
                value: null
            },

            /**
             * The Firebase app object constructed from the other fields of
             * this element.
             * @type {firebase.app.App}
             */
            appID: {
                type: String,
                notify: true,
                computed: 'initApp(name, apiKey, authDomain, databaseUrl, storageBucket, messagingSenderId, projectId, appID)'
            },
        }
    }

    initApp(name, key, domain, url, storage, sender, project, id) {
        if (name && key && domain && url && storage && sender && project) {
            firebase.initializeApp({
                apiKey: key,
                appId: id,
                authDomain: domain,
                databaseURL: url,
                messagingSenderId: sender,
                projectId: project,
                storageBucket: storage
            });
            this.fire('firebase-app-initialized');
        } else {
            return null;
        }

        return firebase.app(name);
    }
}

const FirebaseCommonBehavior = {
    properties: {
        app: {
            type: Object,
            notify: true,
            observer: '__appChanged'
        },

        appName: {
            type: String,
            notify: true,
            value: '',
            observer: '__appNameChanged'
        }
    },

    appNameChanged(appName) {
        if (this.app && this.app.name === appName) {
            return;
        }

        try {
            appName == null ? this.app = firebase.app()
                : this.app = firebase.app(appName);
        } catch (e) {
            // appropriate app hasn't been initialized yet
            var self = this;
            window.addEventListener('firebase-app-initialized',
                onFirebaseAppInitialized = event => {
                    window.removeEventListener(
                        'firebase-app-initialized', onFirebaseAppInitialized);
                    self.__appNameChanged(self.appName);
                });
        }
    },

    appChanged(app) {
        if (app && app.name === this.appName) {
            return;
        }

        this.appName = app ? app.name : '';
    },

    onError(err) {
        this.fire('error', err);
    }
}

class FirebaseAuth extends mixinBehaviors([FirebaseCommonBehavior, AppNetworkStatusBehavior], PolymerElement) {
    static get properties() {
        return {
            /**
             *  [`firebase.auth`](https://firebase.google.com/docs/auth/web/start?authuser=0) service interface.
             */
            auth: {
                type: Object,
                computed: 'initAuth(app)',
                observer: 'appChanged'
            },
            /**
             * Default auth provider OAuth flow to user when attempting provider
             * sign in. This property can remain undefined when attempting to sign
             * in anonymously, using email and password, or when specifying a 
             * provider in the provider sign-in function calls (i.e.
             * `signInWithPopup` and `signInWithRedirect`).
             * 
             * Current accepted providers are:
             * 
             * ```
             * 'google'
             * 'facebook'
             * 'twitter'
             * 'github'
             * 'yahoo'
             * 'microsoft'
             * ```
             */
            provider: {
                type: String,
                notify: true
            },
            /**
             * True if the client is authenticated, otherwise false.
             */
            signedIn: {
                type: Boolean,
                computed: 'isSignedIn(user)',
                notify: true
            },
            /**
             * The currently-authenticated user with user-related metadata. See
             * the ['firebase.User'](https://firebase.google.com/docs/reference/js/firebase.User)
             * documentation for the spec.
             */
            user: {
                type: Object,
                readOnly: true,
                value: null,
                notify: true
            },
            /**
             * When true, login status can be determine by checking `user` property.
             */
            statusKnown: {
                type: Boolean,
                value: false,
                notify: true,
                readOnly: true,
                reflectToAttribute: true
            },
        }
    }

    /**
     * Authenticates a Firebase client using a new, temporary guest account.
     * 
     * @return {Promise} Promise that handles success and failure.
     */
    signInAnonymously() {
        if (!this.auth) {
            return Promise.reject('No app configured for firebase-auth.');
        }

        return this.handleSignIn(this.auth.signInAnonymously());
    }

    /**
     * Authenticates a Firebase client using a custom JSON Web Token.
     * 
     * @return {Promise} Promise that handles success and failure.
     */
    signInWithCustomToken(token) {
        if (!this.auth) {
            return Promise.reject('No app configured for firebase-auth.');
        }

        return this.handleSignIn(this.auth.signInWithCustomToken(token));
    }

    /**
     * Authenticates a Firebase client using an oauth id_token.
     *
     * @return {Promise} Promise that handles success and failure.
     */
    signInWithCredential(credential) {
        if (!this.auth) {
            return Promise.reject('No app configured for firebase-auth!');
        }
        return this.handleSignIn(this.auth.signInWithCredential(credential));
    }

    /**
     * Authenticates a Firebase client using a popup-based OAuth flow.
     *
     * @param  {?String} provider Provider OAuth flow to follow. If no
     * provider is specified, it will default to the element's `provider`
     * property's OAuth flow (See the `provider` property's documentation
     * for supported providers).
     * @return {Promise} Promise that handles success and failure.
     */
    signInWithPopup(provider) {
        return this.attemptProviderSignIn(this.normalizeProvider(provider), this.auth.signInWithPopup);
    }

    /**
     * Authenticates a firebase client using a redirect-based OAuth flow.
     *
     * @param  {?String} provider Provider OAuth flow to follow. If no
     * provider is specified, it will default to the element's `provider`
     * property's OAuth flow (See the `provider` property's documentation
     * for supported providers).
     * @return {Promise} Promise that handles failure. (NOTE: The Promise
     * will not get resolved on success due to the inherent page redirect
     * of the auth flow, but it can be used to handle errors that happen
     * before the redirect).
     */
    signInWithRedirect(provider) {
        return this.attemptProviderSignIn(this.normalizeProvider(provider), this.auth.signInWithRedirect);
    }

    /**
     * Authenticates a Firebase client using an email / password combination.
     *
     * @param  {!String} email Email address corresponding to the user account.
     * @param  {!String} password Password corresponding to the user account.
     * @return {Promise} Promise that handles success and failure.
     */
    signInWithEmailAndPassword(email, password) {
        return this.handleSignIn(this.auth.signInWithEmailAndPassword(email, password));
    }

    /**
     * Creates a new user account using an email / password combination.
     *
     * @param  {!String} email Email address corresponding to the user account.
     * @param  {!String} password Password corresponding to the user account.
     * @return {Promise} Promise that handles success and failure.
     */
    createUserWithEmailAndPassword(email, password) {
        return this.handleSignIn(this.auth.createUserWithEmailAndPassword(email, password));
    }

    /**
     * Sends a password reset email to the given email address.
     *
     * @param  {!String} email Email address corresponding to the user account.
     * @return {Promise} Promise that handles success and failure.
     */
    sendPasswordResetEmail(email) {
        return this.handleSignIn(this.auth.sendPasswordResetEmail(email));
    }

    /**
     * Unauthenticates a Firebase client.
     *
     * @return {Promise} Promise that handles success and failure.
     */
    signOut() {
        if (!this.auth) {
            return Promise.reject('No app configured for auth!');
        }

        return this.auth.signOut();
    }

    attemptProviderSignIn(provider, method) {
        provider = provider || this.providerFromName(this.provider);
        if (!provider) {
            return Promise.reject('Must supply a provider for popup sign in.');
        }
        if (!this.auth) {
            return Promise.reject('No app configured for firebase-auth!');
        }

        return this.handleSignIn(method.call(this.auth, provider));
    }

    providerFromName(name) {
        switch (name) {
            case 'facebook': return new firebase.auth.FacebookAuthProvider();
            case 'github': return new firebase.auth.GithubAuthProvider();
            case 'google': return new firebase.auth.GoogleAuthProvider();
            case 'twitter': return new firebase.auth.TwitterAuthProvider();
            default: this.fire('error', 'Unrecognized firebase-auth provider "' + name + '"');
        }
    }

    normalizeProvider(provider) {
        if (typeof provider === 'string') {
            return this.providerFromName(provider);
        }
        return provider;
    }

    handleSignIn(promise) {
        return promise.catch((err => {
            this.fire('error', err);
            throw err;
        }).bind(this));
    }

    computeSignedIn(user) {
        return !!user;
    }

    computeAuth(app) {
        return this.app.auth();
    }

    authChanged(auth, oldAuth) {
        this.setStatusKnown(false);
        if (oldAuth !== auth && this.unsubscribe) {
            this.unsubscribe();
            this.unsubscribe = null;
        }

        if (this.auth) {
            this.unsubscribe = this.auth.onAuthStateChanged((user => {
                this.setUser(user);
                this.setStatusKnown(true);
            }).bind(this), (err =>
                this.fire('error', err)
            ).bind(this));
        }
    }
}

window.customElements.define('firebase-app', FirebaseApp);
window.customElements.define('firebase-auth', FirebaseAuth);
