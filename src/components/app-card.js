import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';

window.customElements.define('app-card', class AppCard extends PolymerElement {
    static get template() {
        return html`
            <style>
                .card {
                    position: relative;
                    box-sizing: border-box;
                }
                /* Sections
                 ========================================================================== */
                .card-body {
                    padding: 30px 30px;
                }
                .card-header {
                    padding: 15px 30px;
                }
                .card-footer {
                    padding: 15px 30px;
                }
                /* Desktop and bigger */
                @media (min-width: 1200px) {
                    .card-body {
                        padding: 40px 40px;
                    }
                    .card-header {
                        padding: 20px 40px;
                    }
                    .card-footer {
                        padding: 20px 40px;
                    }
                }
                /*
                 * Micro clearfix
                 */
                .card-body::before,
                .card-body::after,
                .card-header::before,
                .card-header::after,
                .card-footer::before,
                .card-footer::after {
                    content: "";
                    display: table;
                }
                .card-body::after,
                .card-header::after,
                .card-footer::after {
                    clear: both;
                }
                /*
                 * Remove margin from the last-child
                 */
                .card-body > :last-child,
                .card-header > :last-child,
                .card-footer > :last-child {
                    margin-bottom: 0;
                }
                /* Media */
                /*
                 * Reserved alignment modifier to style the media element, e.g. with 'border-radius'
                 * Implemented by the theme
                 */
                /* Title */
                .card-title {
                    font-size: 1.5rem;
                    line-height: 1.4;
                }
                /* Badge */
                .card-badge {
                    position: absolute;
                    top: 30px;
                    left: 30px;
                    z-index: 1;
                }
                /*
                 * Remove margin from adjacent element
                 */
                .card-badge:first-child + * {
                    margin-top: 0;
                }
                /* Hover modifier */
                .card-hover:not(.card-default):not(.card-primary):not(.card-secondary):hover {
                    background: #f8f8f8;
                }
                /* Style modifiers */
                /*
                 * Default
                 * Note: Header and Footer are only implemented for the default style
                 */
                .card-default {
                    background: #fff;
                    color: #666;
                    box-shadow: 0 5px 15px rgba(0,0,0,0.08);
                }
                .card-default .card-title {
                    color: #333;
                }
                .card-default.card-hover:hover {
                    background-color: #ebebeb;
                }
                /*
                 * Primary
                 */
                .card-primary {
                    background: #1e87f0;
                    color: #fff;
                }
                .card-primary .card-title {
                    color: #fff;
                }
                .card-primary.card-hover:hover {
                    background-color: #0f7ae5;
                }
                /*
                 * Secondary
                 */
                .card-secondary {
                    background: #222;
                    color: #fff;
                }
                .card-secondary .card-title {
                    color: #fff;
                }
                .card-secondary.card-hover:hover {
                    background-color: #151515;
                }
                /*
                 * Small
                 */
                .card-small.card-body,
                .card-small .card-body {
                    padding: 20px 20px;
                }
                .card-small .card-header {
                    padding: 13px 20px;
                }
                .card-small .card-footer {
                    padding: 13px 20px;
                }
                /*
                 * Large
                 */
                /* Desktop and bigger */
                @media (min-width: 1200px) {
                    .card-large.card-body,
                    .card-large .card-body {
                        padding: 70px 70px;
                    }
                    .card-large .card-header {
                        padding: 35px 70px;
                    }
                    .card-large .card-footer {
                        padding: 35px 70px;
                    }
                }
            </style>

            <div class="card card-small card-default card-body">
                <slot name='title' class='card-title'></slot>
                <slot name='content'></slot>
            </div>
        `;
    }
});
